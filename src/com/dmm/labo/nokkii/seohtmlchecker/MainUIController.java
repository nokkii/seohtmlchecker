package com.dmm.labo.nokkii.seohtmlchecker;

import java.io.File;
import java.util.Optional;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.media.AudioClip;

public class MainUIController {
	// トップURLパネル
	@FXML private Button analyzeBtn;
	@FXML private TextField urlText;

	//タブパネル
	@FXML private Tab allUrlTab;
	@FXML private Tab errorUrlTab;
	@FXML private Tab paramUrlTab;
	private Tab toggle = allUrlTab;
	//表1
    @SuppressWarnings("rawtypes")
    @FXML private TableView allUrlTable;
    @SuppressWarnings("rawtypes")
    @FXML private TableColumn urlCol;
    @SuppressWarnings("rawtypes")
    @FXML private TableColumn referedLineCol;
	//表2
	@SuppressWarnings("rawtypes")
	@FXML private TableView errorUrlTable;
	@SuppressWarnings("rawtypes")
	@FXML private TableColumn errorUrlCol;
	@SuppressWarnings("rawtypes")
	@FXML private TableColumn errorLineCol;
	//表3
	@SuppressWarnings("rawtypes")
	@FXML private TableView paramUrlTable;
	@SuppressWarnings("rawtypes")
	@FXML private TableColumn baseUrlCol;
	@SuppressWarnings("rawtypes")
	@FXML private TableColumn paramsCol;
	@SuppressWarnings("rawtypes")
	@FXML private TableColumn azCol;
	
	//ステータスパネル
	@FXML private Label statusText;
	@FXML private Label progressText;
	@FXML private ProgressBar taskProgress;
	
  public static AudioClip doneSound = new AudioClip(new File("C:\\work\\SEOHTMLChecker\\resource\\decision4.mp3").toURI().toString());
	private SEOHTMLChecker checker;
	
	@FXML
    public void onAnalyzeBtnClicked(ActionEvent event) {
		if (toggle == allUrlTab) {
			checker = new SEOHTMLChecker(urlText.getText(), ()->drawList(checker.getAllURLs()),
					(Double lineCount, Integer numOfLine)->progressUpdate(lineCount, numOfLine));
		} else if (toggle == errorUrlTab) {
			checker = new SEOHTMLChecker(urlText.getText(), ()->drawErrorList(checker.checkURLEndSlash()),
					(Double lineCount, Integer numOfLine)->progressUpdate(lineCount, numOfLine));
		} else {
			// parametersタブ
			checker = new SEOHTMLChecker(urlText.getText(), ()->drawParamList(checker.checkParamAToZ()),
					(Double lineCount, Integer numOfLine)->progressUpdate(lineCount, numOfLine));
		}
    }
	
	@FXML
    public void onTabButtonClicked(Event event) {
		toggle = (Tab) event.getSource();
		Optional.ofNullable(checker).ifPresent((checker) -> {
			if (toggle == allUrlTab) {
				drawList(checker.getAllURLs());
			} else if (toggle == errorUrlTab) {
				drawErrorList(checker.checkURLEndSlash());
			} else {
				drawParamList(checker.checkParamAToZ());
			}
		});
    }
	
    public void progressUpdate(double progressValue, int maxValue) {
		// 外部スレッドから呼ぶので保護
    	Platform.runLater(()->{
        	taskProgress.setProgress(progressValue / maxValue);
        	progressText.setText(String.format("%.0f%%", (progressValue / maxValue * 100)));
        	
    		if (progressValue / maxValue > 0.99) {
            	statusText.setText(":HTMLのパースが完了しました");
              doneSound.play();
            	taskProgress.setProgress(0.0);
            	progressText.setText(":準備完了");
    		} else {
    			statusText.setText("HTMLのパース中");
    		}
    	});
    }
	
	@SuppressWarnings("unchecked")
	private void drawList(AnalyzedURLMap allUrls) {
		// ラムダで外部スレッドから呼ぶので保護
		Platform.runLater(()->{
			ObservableList<TableRecord> tableRecord = FXCollections.observableArrayList();
			for (String url: allUrls.keySet()) {
				tableRecord.add(new TableRecord(url, allUrls.get(url).toString()));
			}
			if (tableRecord.size() > 0) {
				allUrlTable.setItems(tableRecord);
				urlCol.setCellValueFactory(new PropertyValueFactory<TableRecord, String>("url"));
				referedLineCol.setCellValueFactory(new PropertyValueFactory<TableRecord, String>("referedLine"));
				
				allUrlTable.getColumns().setAll(urlCol, referedLineCol);
			}
		});
	}

	@SuppressWarnings("unchecked")
	private void drawErrorList(AnalyzedURLMap errors) {
		// ラムダで外部スレッドから呼ぶので保護
		Platform.runLater(()->{
			ObservableList<TableRecord> tableRecord = FXCollections.observableArrayList();
			for (String url: errors.keySet()) {
				tableRecord.add(new TableRecord(url, errors.get(url).toString()));
			}
			if (tableRecord.size() > 0) {
				errorUrlTable.setItems(tableRecord);
				errorUrlCol.setCellValueFactory(new PropertyValueFactory<TableRecord, String>("url"));
				errorLineCol.setCellValueFactory(new PropertyValueFactory<TableRecord, String>("referedLine"));
				
				errorUrlTable.getColumns().setAll(errorUrlCol, errorLineCol);
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	private void drawParamList(URLParamMap urlParamMap) {
		// ラムダで外部スレッドから呼ぶので保護
		Platform.runLater(()->{
			ObservableList<TableRecord> tableRecord = FXCollections.observableArrayList();
			for (String pureURL: urlParamMap.keySet()) {
				for (String params: urlParamMap.get(pureURL).keySet()) {
					String prettyParamsStr = "";
					for (String param: urlParamMap.get(pureURL).get(params).getParamArray()) {
						prettyParamsStr += param + "   ";
					}
					tableRecord.add(new TableRecord(pureURL, prettyParamsStr, urlParamMap.get(pureURL).get(params).isAZ()));
				}
			}
			if (tableRecord.size() > 0) {
				paramUrlTable.setItems(tableRecord);
				baseUrlCol.setCellValueFactory(new PropertyValueFactory<TableRecord, String>("url"));
				paramsCol.setCellValueFactory(new PropertyValueFactory<TableRecord, String>("referedLine"));
				azCol.setCellValueFactory(new PropertyValueFactory<TableRecord, String>("aZ"));
				
				paramUrlTable.getColumns().setAll(baseUrlCol, paramsCol, azCol);
			}
		});
	}

	public static class TableRecord {
		private StringProperty url;
		private StringProperty referedLines;
		private StringProperty aZ;
		
		public StringProperty urlProperty(){ return url; }
		public StringProperty referedLineProperty(){ return referedLines; }
		public StringProperty aZProperty(){ return aZ; }

		private TableRecord(String url, String referedLine) {
			this.url = new SimpleStringProperty(url);
			this.referedLines = new SimpleStringProperty(referedLine);
		}
		
		private TableRecord(String url, String referedLine, boolean isAZ) {
			this.url = new SimpleStringProperty(url);
			this.referedLines = new SimpleStringProperty(referedLine);
			this.aZ = new SimpleStringProperty(isAZ ? "○" : "×");
		}
	}
}