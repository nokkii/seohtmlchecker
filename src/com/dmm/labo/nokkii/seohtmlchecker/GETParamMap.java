package com.dmm.labo.nokkii.seohtmlchecker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class GETParamMap extends HashMap<String, Object> {
	private static final long serialVersionUID = 7373439384516822362L;
	public GETParamMap(String params) {
		put("Params", params);
		put("ParamArray", new ArrayList<>(Arrays.asList(params.split("&"))));
    String topParam = params.split("&")[0];
		put("ParamCount", topParam.equals("") ? 0 : params.split("&").length);
	}
	
	@SuppressWarnings("unchecked")
	public boolean isAZ() {
		String prevString = ((ArrayList<String>)get("ParamArray")).get(0);
		for (String param: (ArrayList<String>)get("ParamArray")) {
			if (prevString.compareTo(param) > 0) {
				return false;
			}
      prevString = param;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> getParamArray() {
		return (ArrayList<String>)get("ParamArray");
	}
	
	public int getParamCount() {
		return (int)get("ParamCount");
	}
	
	public String getParams() {
		return (String)get("Params");
	}
}
