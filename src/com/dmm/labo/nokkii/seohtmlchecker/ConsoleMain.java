package com.dmm.labo.nokkii.seohtmlchecker;

import java.io.IOException;

public class ConsoleMain {
	private static boolean lock;
	static SEOHTMLChecker checker;

	public static void main(String[] args) {
		try {
			lock = true;
			checker = new SEOHTMLChecker(args[0], ()->{
					drawErrorList(checker.checkURLEndSlash());
					lock = false;
				}, (Double lineCount, Integer numOfLine)->{
					try {
						Runtime.getRuntime().exec("cls");
					} catch (IOException e) {}
					System.out.println((int)(lineCount/numOfLine*100) + "%");
				});
		} catch (Exception e) {
			System.out.println("URLかFilePathをコマンドライン引数に入力してください");
			return;
		}
		
		while(lock) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void drawErrorList(AnalyzedURLMap errors) {
		for (String uri: errors.keySet()) {
			System.out.println(uri + errors.get(uri));
		}
	}
}
