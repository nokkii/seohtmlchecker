package com.dmm.labo.nokkii.seohtmlchecker;
import java.util.HashMap;
import java.util.TreeMap;

/***
 * DataStructure
 *  NonParamURL
 * 		\_ParamCount
 * 				\_ParamName
 * 						\_Params
 * 						\_AZ?
 * 				\_ParamName
 * 						\_Params
 * 						\_AZ?
 * 		\_ParamCount
 * 				\_ParamName
 * 						\_Params
 * 						\_AZ?
 * 				\_ParamName
 * 						\_Params
 * 						\_AZ?
 */

public class URLParamMap extends TreeMap<String, HashMap<String, GETParamMap>> {
	private static final long serialVersionUID = 5880371595771076311L;
}
