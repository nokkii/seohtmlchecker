package com.dmm.labo.nokkii.seohtmlchecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

import org.jsoup.*;
import org.jsoup.nodes.Document;

public class PathAnalyzer {
	public String encoding = "UTF-8";
	private String evaluateUrl;
	
	public PathAnalyzer(String evaluateUrl){
		this.evaluateUrl = evaluateUrl;
	}

	// 文字列がURLかファイルパスか判定してBufferedReaderを返す
	public BufferedReader BRGenerator() throws IOException, UnknownHostException, FileNotFoundException {
    if (evaluateUrl.matches("https?://[\\w/:%#\\$&\\?\\(\\)~\\.=\\+\\-]+")) {
      URL url = new URL(evaluateUrl);
      URLConnection conn = url.openConnection();
      return new BufferedReader(new InputStreamReader(conn.getInputStream()));
    } else {
      File file;
      file = new File(evaluateUrl);
      return new BufferedReader(new FileReader(file));
    }
	}
	
	// 文字列がURLかファイルパスか判定してDOMを返す
	public Document DOMTreeGenerator() {
		try {
			if (evaluateUrl.matches("https?://[\\w/:%#\\$&\\?\\(\\)~\\.=\\+\\-]+")) {
				return Jsoup.connect(evaluateUrl).get();
			} else {
				File file;
				file = new File(evaluateUrl);
				return Jsoup.parse(file, encoding);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
