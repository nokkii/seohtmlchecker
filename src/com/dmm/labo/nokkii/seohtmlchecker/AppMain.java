package com.dmm.labo.nokkii.seohtmlchecker;
	
import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.fxml.FXMLLoader;


public class AppMain extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("SEOHTMLChecker");
		try {
			VBox root = (VBox)FXMLLoader.load(getClass().getResource("MainUI.fxml"));
			Scene scene = new Scene(root,800,600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
      
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
