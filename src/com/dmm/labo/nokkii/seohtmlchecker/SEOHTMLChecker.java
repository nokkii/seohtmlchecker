package com.dmm.labo.nokkii.seohtmlchecker;

import java.util.HashMap;
import java.util.function.BiConsumer;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class SEOHTMLChecker {
	public AnalyzedURLMap urlLineNumMap;
	public Elements links;
	private Document doc;
	private PathAnalyzer analyzer;
	
	public SEOHTMLChecker(String evaluateUrl, Runnable callback, BiConsumer<Double, Integer> progress) {
		// URL判定
		analyzer = new PathAnalyzer(evaluateUrl);
		doc = analyzer.DOMTreeGenerator();
		urlLineNumMap = new AnalyzedURLMap();
		
		links = doc.getElementsByTag("a");
		HTMLParser parser = new HTMLParser(analyzer, links, urlLineNumMap, callback, progress);
		new Thread(parser).start();
	}

	public AnalyzedURLMap getAllURLs() { return urlLineNumMap; }

	public AnalyzedURLMap checkURLEndSlash() {
		AnalyzedURLMap errorWithElements = new AnalyzedURLMap();
		
		for (String uri: urlLineNumMap.keySet()) {
			/***********************************************************/
			/**********  #始まり、.html、/終わりを弾く     　　　　　　　　 **************/
			/***********************************************************/
			if (! (uri.matches("(.*/)") || uri.matches("^#.*")) || uri.matches(".*\\.html/*$")) {
				errorWithElements.put(uri, urlLineNumMap.get(uri));
			}
		}
		
		return errorWithElements;
	}
	
	public URLParamMap checkParamAToZ() {
		URLParamMap urlParamMap = new URLParamMap();
		
		for (String uri: urlLineNumMap.keySet()) {
			if (uri.matches(".*\\?.*")) {
				String[] splittedURI = uri.split("\\?");
				String pureURL = splittedURI[0];
				GETParamMap param = new GETParamMap(splittedURI[1]);
				
				HashMap<String, GETParamMap> paramWithInfo = new HashMap<String, GETParamMap>();
				if ((paramWithInfo = urlParamMap.get(pureURL)) == null) {
					paramWithInfo = new HashMap<String, GETParamMap>();
				}
				paramWithInfo.put(param.getParams(), param);
				urlParamMap.put(pureURL, paramWithInfo);
			}
		}
		
		return urlParamMap;
	}
}
