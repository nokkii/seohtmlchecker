package com.dmm.labo.nokkii.seohtmlchecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.BiConsumer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HTMLParser extends Thread{
	private PathAnalyzer analyzer;
	private Elements links;
	private AnalyzedURLMap urlLineNumMap;
	private Runnable callback;
	private BiConsumer<Double, Integer> progress;

	public HTMLParser(PathAnalyzer analyzer, Elements links, AnalyzedURLMap urlLineNumMap, Runnable callback, BiConsumer<Double, Integer> progress) {
		this.analyzer = analyzer;
		this.links = links;
		this.urlLineNumMap = urlLineNumMap;
		this.callback = callback;
		this.progress = progress;
	}
	
	@Override
	public void run() {
		try {
			BufferedReader br = analyzer.BRGenerator();
			int numOfLine = 0;
			for (; br.readLine() != null; numOfLine++);
			br = analyzer.BRGenerator();
			String line;
			
			for (Integer lineCount = 0; (line = br.readLine()) != null; lineCount++) {
				progress.accept(lineCount.doubleValue() + 1.0, numOfLine);

				for (Element aTag: links) {
					String uri = aTag.attr("href");
					if (line.matches(".*\\\"" + uri.replaceAll("\\?", "\\\\?") + "\\\".*")) {
						ArrayList<Integer> lineNumbers;
						if ((lineNumbers = urlLineNumMap.get(uri)) == null) {
							lineNumbers = new ArrayList<Integer>();
						}
						lineNumbers.add(lineCount);
						urlLineNumMap.put(uri, lineNumbers);
					}
				}
			}
			callback.run();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
