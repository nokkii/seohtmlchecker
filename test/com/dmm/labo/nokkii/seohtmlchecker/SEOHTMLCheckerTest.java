package com.dmm.labo.nokkii.seohtmlchecker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import static org.hamcrest.CoreMatchers.*;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.Assert.*;

import org.junit.Test;


public class SEOHTMLCheckerTest {
  private static final String HTML_PATH = "C:\\work\\SEOHTMLChecker\\src\\com\\dmm\\labo\\nokkii\\seohtmlchecker\\test.html";
	////// GETParamMap
	@Test
	public void GETParamMap_isAZ_昇順と順不同のパラメーターを区別できる() {
		GETParamMap params1 = new GETParamMap("a=0&b=0&c=0&d=0&e=0");
		GETParamMap params2 = new GETParamMap("a=0&f=0&c=0&d=0&e=0");
		assertThat(params1.isAZ(), is(true));
		assertThat(params2.isAZ(), is(false));
	}
	
	@Test
	public void GETParamMap_getParamArray_入力した文字列と同じ並びのCollectionができる() {
		GETParamMap params1 = new GETParamMap("a=0&b=0&c=0&d=0&e=0");
		GETParamMap params2 = new GETParamMap("a=0&f=0&c=0&d=0&e=0");
		assertArrayEquals(params1.getParamArray().toArray(), Arrays.asList("a=0", "b=0", "c=0", "d=0", "e=0").toArray());
		assertArrayEquals(params2.getParamArray().toArray(), Arrays.asList("a=0", "f=0", "c=0", "d=0", "e=0").toArray());
	}
	
	@Test
	public void GETParamMap_getParamCount_入力したパラメータ数が取得できる() {
		GETParamMap params1 = new GETParamMap("a=0&b=0&c=0&d=0&e=0");
		GETParamMap params2 = new GETParamMap("");
		assertThat(params1.getParamCount(), is(5));
		assertThat(params2.getParamCount(), is(0));
	}
	
	@Test
	public void GETParamMap_getParams_入力したパラメータ文字列がそのまま取れる() {
		GETParamMap params1 = new GETParamMap("a=0&b=0&c=0&d=0&e=0");
		GETParamMap params2 = new GETParamMap("a=0&f=0&c=0&d=0&e=0");
		assertThat(params1.getParams(), is("a=0&b=0&c=0&d=0&e=0"));
		assertThat(params2.getParams(), is("a=0&f=0&c=0&d=0&e=0"));
	}
	
	////// HTMLPaser
	@Test
	public void HTMLPaser_run_URLと登場行が正解データと一致する() {
    PathAnalyzer analyzer = new PathAnalyzer(HTML_PATH);
    Document doc = analyzer.DOMTreeGenerator();
    Elements links = doc.getElementsByTag("a");
    AnalyzedURLMap urlLineNumMap = new AnalyzedURLMap();
    
    HTMLParser parser = new HTMLParser(analyzer, links, urlLineNumMap, ()->{
      // callbackに以後のテストの処理を書かないとデータが同期できない
      assertArrayEquals(
              urlLineNumMap.keySet().toArray(),
              Arrays.asList(
                      "http://www.dmm.com",
                      "http://www.dmm.com/",
                      "http://www.dmm.com/index.html",
                      "http://www.dmm.com/index.html/",
                      "https://www.google.co.jp/webhp?espv=2&ie=UTF-8&ion=1#q=JavaFX8",
                      "https://www.google.co.jp/webhp?espv=2&ie=UTF-8&ion=1&sourceid=chrome-instant#q=JavaFX8",
                      "https://www.google.co.jp/webhp?ion=1&espv=2&ie=UTF-8#q=JavaFX8",
                      "https://www.google.co.jp/webhp?ion=1&espv=2&ie=UTF-8&sourceid=chrome-instant#q=JavaFX8"
              ).toArray());

      Object[] lineNumArray = Arrays.asList(
              Arrays.asList(5).toArray(),
              Arrays.asList(3).toArray(),
              Arrays.asList(7).toArray(),
              Arrays.asList(9).toArray(),
              Arrays.asList(13).toArray(),
              Arrays.asList(11).toArray(),
              Arrays.asList(17).toArray(),
              Arrays.asList(15).toArray()
      ).toArray();
      Iterator<ArrayList<Integer>> targetlineNumArray = urlLineNumMap.values().iterator();
      for (Object numArray: lineNumArray) {
        assertArrayEquals(targetlineNumArray.next().toArray(),(Integer[])numArray);
      }
    
    }, (Double lineCount, Integer numOfLine)->System.out.print(""));
		new Thread(parser).start();
	}
  
	@Test
	public void PathAnalyzer_BRGenerator_有効無効なURLPathを受け取る() {
    try {
      //URL
      new PathAnalyzer("http://www.dmm.com/").BRGenerator();
      
      try {
        //無効なURL
        new PathAnalyzer("http://www.dmm.c").BRGenerator();
      } catch (UnknownHostException e) {
        assertThat(true, is(true));
      }
      
      try {
        //無効なPath
        new PathAnalyzer("abcd").BRGenerator();
      } catch (FileNotFoundException e) {
        assertThat(true, is(true));
      }
      
      //Path
      BufferedReader br = new PathAnalyzer(HTML_PATH).BRGenerator();
      assertThat(br.readLine(), is("<html>"));
    } catch (IOException ex) {
    }
	}
	
	@Test
	public void PathAnalyzer_DOMTreeGenerator_DOMツリーを作成() {
    PathAnalyzer analyzer = new PathAnalyzer(HTML_PATH);
    Document doc = analyzer.DOMTreeGenerator();
    Elements links = doc.getElementsByTag("a");
		assertThat(links.first().attr("href"), is("http://www.dmm.com/"));
	}
	
	////// SEOHTMLChecker
  public static SEOHTMLChecker checker;
	@Test
	public void SEOHTMLChecker_getAllURLs_メソッドの実行() {
    checker = new SEOHTMLChecker(HTML_PATH, ()->{
      AnalyzedURLMap allURL = checker.getAllURLs();
      assertThat(true, is(true));
    }, (Double lineCount, Integer numOfLine)->System.out.print(""));
	}
	
	@Test
	public void SEOHTMLChecker_checkURLEndSlash_メソッドの戻り値が適切か() {
    checker = new SEOHTMLChecker(HTML_PATH, ()->{
      // callbackに以後のテストの処理を書かないとデータが同期できない
      assertArrayEquals(
              checker.checkURLEndSlash().keySet().toArray(),
              Arrays.asList(
                      "http://www.dmm.com",
                      "http://www.dmm.com/index.html",
                      "http://www.dmm.com/index.html/",
                      "https://www.google.co.jp/webhp?espv=2&ie=UTF-8&ion=1#q=JavaFX8",
                      "https://www.google.co.jp/webhp?espv=2&ie=UTF-8&ion=1&sourceid=chrome-instant#q=JavaFX8",
                      "https://www.google.co.jp/webhp?ion=1&espv=2&ie=UTF-8#q=JavaFX8",
                      "https://www.google.co.jp/webhp?ion=1&espv=2&ie=UTF-8&sourceid=chrome-instant#q=JavaFX8"
              ).toArray());

      Object[] lineNumArray = Arrays.asList(
              Arrays.asList(5).toArray(),
              Arrays.asList(7).toArray(),
              Arrays.asList(9).toArray(),
              Arrays.asList(13).toArray(),
              Arrays.asList(11).toArray(),
              Arrays.asList(17).toArray(),
              Arrays.asList(15).toArray()
      ).toArray();
      Iterator<ArrayList<Integer>> targetlineNumArray = checker.checkURLEndSlash().values().iterator();
      for (Object numArray: lineNumArray) {
        assertArrayEquals(targetlineNumArray.next().toArray(),(Integer[])numArray);
      }
      
    }, (Double lineCount, Integer numOfLine)->System.out.print(""));
	}
	
	@Test
	public void SEOHTMLChecker_checkParamAToZ_URLリストと正解データの比較() {
		AnalyzedURLMap urlLineNumMap = new AnalyzedURLMap();
    checker = new SEOHTMLChecker(HTML_PATH, ()->{
      // callbackに以後のテストの処理を書かないとデータが同期できない
      assertArrayEquals(
              checker.checkParamAToZ().keySet().toArray(),
              Arrays.asList(
                      "https://www.google.co.jp/webhp"
              ).toArray());
    }, (Double lineCount, Integer numOfLine)->System.out.print(""));
	}
}